FROM python:3.6.8-stretch

ENV DEBIAN_FRONTEND noninteractive
RUN mkdir -p /ws
WORKDIR ws
COPY requirements.txt /ws/
RUN pip install -r requirements.txt
COPY main.py /ws/
CMD ["python", "./main.py"]
