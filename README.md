# Ring Fit Adventure Monitor

This program will monitor and check if ring fit adventure is in stock on amazon and nintendo store.

# Usage

1. install docker and pushbullet
    - https://www.docker.com/
    - https://chrome.google.com/webstore/detail/pushbullet/chlffgpmiacpedhhbkiomidkjlcfhogd?hl=ja
2. Access to https://pushbullet.com/ -> Settings -> Account and create your access token.
   And then, copy the token into `.env` file like the following.

```sh
API_TOKEN=XXXXXXX
```


3. build docker image
```sh
docker build xxx/ringfitadventuremonitor .

```
3. run it
```sh
docker run --rm -it --restart=always -v $(pwd)/.env:/ws/.env xxx/ringfitadventuremonitor 

```
