#!/usr/bin/env python
import requests
import json
import os
from bs4 import BeautifulSoup
import datetime

from dotenv import load_dotenv
import logging
import time
logging.basicConfig(format='[%(levelname)s] [%(asctime)s] [%(name)s: %(funcName)s] %(message)s', level=logging.DEBUG, datefmt='%Y/%m/%d %I:%M:%S %p')
log = logging.getLogger(__name__)


dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)


def push_message(title, body):
    token = os.environ.get('API_TOKEN')
    url = "https://api.pushbullet.com/v2/pushes"
    headers = {"content-type": "application/json", "Authorization": 'Bearer '+token}
    data_send = {"type": "note", "title": title, "body": body}

    log.debug('request:')
    log.debug(json.dumps(data_send))
    r = requests.post(url, headers=headers, data=json.dumps(data_send))
    log.debug('response:')
    log.debug(r)

def check(amazon_url):
    my_header = {
        "User-Agent" : "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; NP06; rv:11.0) like Gecko"
    }

    data = requests.get(amazon_url, headers = my_header)
    data.encoding = data.apparent_encoding
    data = data.text
    soup = BeautifulSoup(data, "html.parser")
    if soup.find("span", id="priceblock_ourprice") is None:
        log.debug("価格がない")
        return False
    anode = soup.find("div",id="merchant-info").find("a")
    if anode is None:
        log.debug('aがない') # デバッグ
        return False
    detail = anode.text
    log.debug('detail: {0}'.format(detail)) # デバッグ
    if 'Amazon' in detail: # Amazon.co.jpが販売なら在庫あり
        return True
    return False


def check_nintendostore(url):
    my_header = {
        "User-Agent" : "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; NP06; rv:11.0) like Gecko"
    }

    data = requests.get(url, headers = my_header)
    data.encoding = data.apparent_encoding
    data = data.text
    soup = BeautifulSoup(data, "html.parser")
    p = soup.select_one('#itemDetail > div.content_area > div.item-form > form > div.item-cart-and-wish-button-area > div.item-cart-add-area > div > p')
    if p is None:
        log.debug('nintendo: pがない') # デバッグ
        return False
    detail = p.text
    log.debug('nintendo detail: {0}'.format(detail)) # デバッグ
    if '品切れ' in detail:
        return False
    return True

if __name__ == '__main__':
    amazon_urls = {
            u'リングフィットアドベンチャー': "https://www.amazon.co.jp/gp/product/B07XV8VSZT/ref=as_li_tl?ie=UTF8&camp=247&creative=1211&creativeASIN=B07XV8VSZT&linkCode=as2&tag=tyounouryokug-22&linkId=af4d67f116ab5dbe6150321c544a3aa6"
    }
    nintendo_urls = {
            u'リングフィットアドベンチャー': "https://store.nintendo.co.jp/category/SWITCH_SOFT/HAC_R_AL3PA.html"
            }
    cnt = 0
    while True:
        log.debug('checkはじめ')
        starttime = time.time()
        for title, url in amazon_urls.items():
            ret = check(url)
            if ret:
                log.debug('{0}が売ってそう'.format(title))
                push_message(title, '売ってるかも? ' + url)
            else:
                log.debug('amazonに{0}は売ってなさそう'.format(title))
        if cnt % 4 == 0:
            try:
                for title, url in nintendo_urls.items():
                    ret = check_nintendostore(url)
                    if ret:
                        log.debug('{0}が売ってそう'.format(title))
                        push_message(title, '売ってるかも? ' + url)
                    else:
                        log.debug('nintendoに{0}は売ってなさそう'.format(title))
            except Exception as e:
                log.debug(e)
        log.debug('checkおわり')
        cnt += 1
        log.debug('待つ')
        time.sleep(starttime + 2*60 - time.time())



